/*
 * Kitchen Sink Bug RTS
 *
 * Copyright (c) 2024  Victor Zhavoronkov
 *
 * This file is part of Kitchen Sink Bug RTS.
 *
 * Kitchen Sink Bug RTS is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Kitchen Sink Bug RTS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Foobar. If not, see <https://www.gnu.org/licenses/>.
 */

import { Locator } from '@playwright/test';

export class Component {
    public locator: Locator
    public parent?: Component

    constructor(locator: Locator, parent?: Component) {
        this.locator = locator
        this.parent = parent
    }

    evaluate<T>(expression: (cmp: any, argument?: any) => T, argument?: any) {
        //@ts-ignore
        const code = (el, arg) => { const cmp = Ext.getCmp(el.id), expression = new Function('const __fn = ' + arg[0] + '; __fn(arguments[0], arguments[1]);'); return expression(cmp, arg[1]); };
        return this.locator.evaluate(code, [expression.toString(), argument])
    }
}