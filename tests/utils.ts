/*
 * Kitchen Sink Bug RTS
 *
 * Copyright (c) 2024  Victor Zhavoronkov
 *
 * This file is part of Kitchen Sink Bug RTS.
 *
 * Kitchen Sink Bug RTS is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Kitchen Sink Bug RTS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Foobar. If not, see <https://www.gnu.org/licenses/>.
 */

import { Page } from '@playwright/test'

export function waitForLoad(waiter: () => Promise<any>, page: Page, timeout: number = 30000) {
    const timeoutPromise = new Promise((_, reject) => {
        setTimeout(() => reject(`Timed out waiting for page load condition in ${timeout}ms`), timeout)
    })
    const loadFn = async () => {
        while (true) {
            try {
                await waiter()
                return
            }
            catch (e) {

            }
        }
    }
    return Promise.race([loadFn(), timeoutPromise])
}

export function getKitchenSinkPageUrl(version: string, page: string) {
    return `https://examples.sencha.com/extjs/${version}/examples/kitchensink/frame-index.html?classic#${page}`
}