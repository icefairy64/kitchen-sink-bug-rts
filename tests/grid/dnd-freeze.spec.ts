/*
 * Kitchen Sink Bug RTS
 *
 * Copyright (c) 2024  Victor Zhavoronkov
 *
 * This file is part of Kitchen Sink Bug RTS.
 *
 * Kitchen Sink Bug RTS is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Kitchen Sink Bug RTS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Foobar. If not, see <https://www.gnu.org/licenses/>.
 */

import { expect, test } from '@playwright/test'
import { Grid } from '../../components/grid'
import { dragTo } from '../../components/utils'
import { extVersions } from '../ext-versions'
import { getKitchenSinkPageUrl, waitForLoad } from '../utils'

// TODO: maybe some steps are redundant, to be investigated

test.describe('grid', () => {
    for (const version of extVersions) {
        test(`[${version}] should not freeze on dnd to the bottom`, async ({page}) => {
            // Open the page
            await waitForLoad(async () => {
                await page.goto(getKitchenSinkPageUrl(version, 'dd-grid-to-grid'))
                await page.locator('.x-grid').first().waitFor({ timeout: 10000 })
            }, page)

            const firstGrid = new Grid(page.locator('.x-grid').nth(0)),
                secondGrid = new Grid(page.locator('.x-grid').nth(1))

            // Add more records to the store
            await firstGrid.evaluate(cmp => {
                const store = cmp.store;
                for (let i = 11; i < 1000; i++) {
                    store.add({name: 'Rec ' + i + '|', column1: i, column2: i});
                }
            })

            // Click on the first record
            const rec1 = firstGrid.getFirstRow()
            await rec1.locator.click()

            // Select records 1-50
            let rec50 = await firstGrid.scrollUntil(() => firstGrid.getRowByText('Rec 50|'))
            await rec50.locator.click({modifiers: ['Shift']})

            // Drag records over the second grid
            await dragTo(rec50, secondGrid.getViewLocator(), {wiggle: true})

            // Scroll rows into view
            rec50 = await secondGrid.scrollUntil(() => secondGrid.getRowByText('Rec 50|'))
            const rec999 = await firstGrid.scrollUntil(() => firstGrid.getRowByText('Rec 999|'), 400)

            // Drag the last record from second grid to the bottom of the first one
            await dragTo(rec50, rec999, {
                edge: 'bottom',
                wiggle: true,
                timeout: 1000
            })

            // Expected: the above drag operation is completed and the record can be found in the first grid
            expect(firstGrid.getRowByText('Rec 50|')).not.toBeNull()
        })
    }
})