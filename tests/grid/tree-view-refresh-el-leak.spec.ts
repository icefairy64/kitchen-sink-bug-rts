/*
 * Kitchen Sink Bug RTS
 *
 * Copyright (c) 2024  Victor Zhavoronkov
 *
 * This file is part of Kitchen Sink Bug RTS.
 *
 * Kitchen Sink Bug RTS is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Kitchen Sink Bug RTS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Foobar. If not, see <https://www.gnu.org/licenses/>.
 */

import { expect, test } from '@playwright/test'
import { extVersions } from '../ext-versions'
import { getKitchenSinkPageUrl, waitForLoad } from '../utils'
import { Component } from '../../components/component'

test.describe('grid', () => {
    for (const version of extVersions) {
        test(`[${version}] should not leak elements on refresh`, async ({page}) => {
            // Open the page
            await waitForLoad(async () => {
                await page.goto(getKitchenSinkPageUrl(version, 'tree-grid'))
                await page.locator('.x-tree-view').first().waitFor({ timeout: 10000 })
            }, page)

            // Wait for data to load
            await page.locator('#loadingSplash').waitFor({
                state: 'detached'
            })

            await page.locator('.x-mask').waitFor({
                state: 'hidden'
            })

            const viewLocator = page.locator('.x-tree-view')
                .filter({
                    hasText: 'Project: Shopping'
                })

            const treeView = new Component(viewLocator)

            const elCountBefore = await viewLocator.locator('*').count()

            await treeView.evaluate(cmp => cmp.refresh())

            const elCountAfter = await viewLocator.locator('*').count()

            expect(elCountAfter).toBeGreaterThan(0)
            expect(elCountAfter).toBe(elCountBefore)
        })
    }
})