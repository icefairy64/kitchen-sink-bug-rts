/*
 * Kitchen Sink Bug RTS
 *
 * Copyright (c) 2024  Victor Zhavoronkov
 *
 * This file is part of Kitchen Sink Bug RTS.
 *
 * Kitchen Sink Bug RTS is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Kitchen Sink Bug RTS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Foobar. If not, see <https://www.gnu.org/licenses/>.
 */

import { Component } from './component'
import { getElementId } from './utils'
import { Locator } from '@playwright/test'

export class Grid extends Component {
    getItemContainerLocator() {
        return this.locator.locator('.x-grid-item-container')
    }

    getHeaderContainerLocator() {
        return this.locator.locator('.x-grid-header-ct')
    }

    getViewLocator() {
        return this.locator.locator('.x-grid-view')
    }

    getColumnHeaderLocator(title: string, groupHeader: boolean = false) {
        const headerCt = this.getHeaderContainerLocator(),
            extraSelector = groupHeader ? '.x-group-header' : ':not(.x-group-header)'
        return headerCt.locator(`.x-column-header${extraSelector}`, {
            has: this.locator.page().getByText(title, { exact: true })
        })
    }

    createRow(locator: Locator) {
        return new GridRow(locator, this)
    }

    async getRowByText(text: string) {
        const locator = this.getItemContainerLocator().locator('table', {
            hasText: text
        })
        if (await locator.count() === 0) {
            return null
        }
        return this.createRow(locator)
    }

    getFirstRow() {
        const locator = this.getItemContainerLocator().locator('table').first()
        return this.createRow(locator)
    }

    getLastRow() {
        const locator = this.getItemContainerLocator().locator('table').last()
        return this.createRow(locator)
    }

    async scrollUntil<T>(matcher: () => Promise<T>, stride: number = 100): Promise<NonNullable<T>> {
        const scroller = this.getViewLocator()
        while (true) {
            const result = await matcher()
            if (result != null) {
                return result
            }
            await scroller.evaluate((el, stride) => el.scrollBy(0, stride), stride)
        }
    }
}

export class GridRow extends Component {
    async getCellLocator(column: string) {
        const grid = this.parent as Grid,
            columnHeader = grid.getColumnHeaderLocator(column),
            columnId = await getElementId(columnHeader)

        return this.locator.locator(`.x-grid-cell[data-columnid="${columnId}"]`)
    }
}