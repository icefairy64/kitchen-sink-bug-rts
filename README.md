# Kitchen Sink Bug RTS

This is a reproduction test suite for a set of bugs personally encountered in Ext JS throughout the years.

All the tests are run against the official [Kitchen Sink](https://examples.sencha.com/extjs/7.7.0/examples/kitchensink/)
example as to not require proprietary code to run and to ensure that the results are not affected by external factors.

## Current status

The latest test results with the recorded Playwright traces are available [here](https://icefairy64.gitlab.io/kitchen-sink-bug-rts/).

## License

Test code in this repository is licensed under GPLv3.

[Ext JS](https://www.sencha.com/products/extjs/) is a proprietary product developed by Sencha Inc.

## How to run

### Requirements

- Node.js (tested against v20.2.0)

### Setup

```shell
npm install            # (or equivalent in your package manager)
npx playwright install # (or equivalent in your package manager)
```

### Running tests

```shell
npx playwright test # (or equivalent in your package manager)
```