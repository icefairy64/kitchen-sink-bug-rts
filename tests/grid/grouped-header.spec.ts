/*
 * Kitchen Sink Bug RTS
 *
 * Copyright (c) 2024  Victor Zhavoronkov
 *
 * This file is part of Kitchen Sink Bug RTS.
 *
 * Kitchen Sink Bug RTS is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Kitchen Sink Bug RTS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Foobar. If not, see <https://www.gnu.org/licenses/>.
 */

import { expect, test } from '@playwright/test'
import { extVersions } from '../ext-versions'
import { getKitchenSinkPageUrl, waitForLoad } from '../utils'
import { Grid } from '../../components/grid'
import { getElementId } from '../../components/utils'

test.describe('grid', () => {
    for (const version of extVersions) {
        test(`[${version}] should not add first / last classes to the group header children cells`, async ({page}) => {
            // Load the page
            await waitForLoad(async () => {
                await page.goto(getKitchenSinkPageUrl(version, 'grouped-header-grid'))
                await page.locator('.x-grid').first().waitFor({ timeout: 10000 })
            }, page)

            // Find the first and last grouped header cells
            const grid = new Grid(page.locator('.x-grid').first())

            const row = grid.getFirstRow(),
                firstCell = await row.getCellLocator('Price'),
                lastCell = await row.getCellLocator('% Change')

            expect(await firstCell.evaluate(el => el.classList.contains('x-grid-cell-first')))
                .not.toBe(true)

            expect(await lastCell.evaluate(el => el.classList.contains('x-grid-cell-last')))
                .not.toBe(true)

            // Click on the group header
            const groupHeader = grid.getColumnHeaderLocator('Stock Price', true),
                groupHeaderId = await getElementId(groupHeader)

            await page.locator(`#${groupHeaderId}-titleEl`).hover()
            await page.locator(`#${groupHeaderId}-triggerEl`).click()

            // Click on a child header to trigger the bug
            const firstChildHeader = grid.getColumnHeaderLocator('Price')
            await firstChildHeader.click()

            expect(await firstCell.evaluate(el => el.classList.contains('x-grid-cell-first')))
                .not.toBe(true)

            expect(await lastCell.evaluate(el => el.classList.contains('x-grid-cell-last')))
                .not.toBe(true)
        })
    }
})