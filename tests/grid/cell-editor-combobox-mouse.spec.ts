/*
 * Kitchen Sink Bug RTS
 *
 * Copyright (c) 2024  Victor Zhavoronkov
 *
 * This file is part of Kitchen Sink Bug RTS.
 *
 * Kitchen Sink Bug RTS is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Kitchen Sink Bug RTS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Foobar. If not, see <https://www.gnu.org/licenses/>.
 */

import { test } from '@playwright/test'
import { extVersions } from '../ext-versions'
import { getKitchenSinkPageUrl, waitForLoad } from '../utils'
import { Grid } from '../../components/grid'

test.describe('grid', () => {
    for (const version of extVersions) {
        test(`[${version}] should show options list when clicking on combobox trigger in cell editor`, async ({page}) => {
            // Load the page
            await waitForLoad(async () => {
                await page.goto(getKitchenSinkPageUrl(version, 'cell-editing'))
                await page.locator('.x-grid').first().waitFor({ timeout: 10000 })
            }, page)

            // Find the cell with a combobox cell editor
            const grid = new Grid(page.locator('.x-grid').first()),
                row = grid.getFirstRow(),
                cell = await row.getCellLocator('Light')

            // Activate the cell
            await cell.click()

            // Click on the combobox trigger to open the option list
            await cell.locator('.x-form-trigger').click()

            // Expected: option list is visible
            await page.locator('.x-boundlist-floating').waitFor({
                state: 'visible',
                timeout: 1000
            })
        })
    }
})