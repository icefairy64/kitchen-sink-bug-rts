/*
 * Kitchen Sink Bug RTS
 *
 * Copyright (c) 2024  Victor Zhavoronkov
 *
 * This file is part of Kitchen Sink Bug RTS.
 *
 * Kitchen Sink Bug RTS is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Kitchen Sink Bug RTS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Foobar. If not, see <https://www.gnu.org/licenses/>.
 */

import { Component } from './component'
import { Locator } from '@playwright/test'

export type BoxEdge = 'top' | 'bottom' | 'left' | 'right' | 'center'

interface DragOptions {
    wiggle?: boolean,
    timeout?: number,
    edge?: BoxEdge
}

export async function dragTo(source: Component | Locator, target: Component | Locator, options?: DragOptions) {
    const sourceLocator = source instanceof Component ? source.locator : source,
        targetLocator = target instanceof Component ? target.locator : target

    await sourceLocator.scrollIntoViewIfNeeded()
    await targetLocator.scrollIntoViewIfNeeded()

    const targetBox = await targetLocator.boundingBox()

    let targetOffsetX: number = 0,
        targetOffsetY: number = 0

    const targetEdge = options?.edge ?? 'center'

    switch (targetEdge) {
        case 'top':
            targetOffsetX = 4
            targetOffsetY = 4
            break
        case 'bottom':
            targetOffsetX = 4
            targetOffsetY = targetBox.height - 4
            break
        case 'left':
            targetOffsetX = 4
            targetOffsetY = 4
            break
        case 'right':
            targetOffsetX = targetBox.width - 4
            targetOffsetY = 4
            break
        case 'center':
            targetOffsetX = targetBox.width / 2
            targetOffsetY = targetBox.height / 2
            break
    }

    if (options?.wiggle) {
        const sourceBox = await sourceLocator.boundingBox(),
            originX = sourceBox.x + sourceBox.width / 2,
            originY = sourceBox.y + sourceBox.height / 2,
            targetX = targetBox.x + targetOffsetX,
            targetY = targetBox.y + targetOffsetY,
            mouse = sourceLocator.page().mouse

        const dragFn = async () => {
            await mouse.move(originX, originY)
            await mouse.down()
            await mouse.move(targetX, targetY)
            await mouse.move(targetX + 1, targetY)
            await mouse.move(targetX, targetY)
            await mouse.up()
        }

        if (options?.timeout != null) {
            const timeoutPromise = new Promise((_, reject) => {
                setTimeout(() => {
                    reject(`Drag-and-drop timed out after ${options.timeout}ms`)
                }, options.timeout)
            })
            await Promise.race([dragFn(), timeoutPromise])
        } else {
            await dragFn()
        }
    } else {
        await sourceLocator.dragTo(targetLocator, {
            timeout: options?.timeout,
            targetPosition: {
                x: targetOffsetX,
                y: targetOffsetY
            }
        })
    }
}

export function getElementId(locator: Locator): Promise<string> {
    return locator.evaluate(el => el.id)
}