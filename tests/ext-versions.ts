/*
 * Kitchen Sink Bug RTS
 *
 * Copyright (c) 2024  Victor Zhavoronkov
 *
 * This file is part of Kitchen Sink Bug RTS.
 *
 * Kitchen Sink Bug RTS is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Kitchen Sink Bug RTS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Foobar. If not, see <https://www.gnu.org/licenses/>.
 */

export const extVersions = ['6.7.0', '7.0.0', '7.1.0', '7.2.0', '7.3.0', '7.4.0', '7.5.0', '7.6.0', '7.7.0', '7.8.0']